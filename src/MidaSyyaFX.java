import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

/**
 * Created by Markus on 2.12.2016.
 */
public class MidaSyyaFX extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception {


        ArrayList<String> kylmkapiSisu = new ArrayList<String>();
        ObservableList kylmkapiSisuFX = FXCollections.observableArrayList();
        ArrayList<String> Toidud = new ArrayList<String>();
        ObservableList ToidudFX = FXCollections.observableArrayList();

        VBox vbox = new VBox();
        HBox hbox = new HBox();
        Scene scene = new Scene(vbox, 700, 900);
        primaryStage.setScene(scene);

        Hyperlink hyperlink = new Hyperlink("Lisa retsepte");
        TextFlow flow = new TextFlow(hyperlink);
        Label label1 = new Label();
        ListView kylmkapiSisuListView = new ListView();
        ListView ToidudListView = new ListView();
        TextField TextBoxSisesta1 = new TextField();
        TextArea TextBoxRetsept = new TextArea();
        Button Button1 = new Button();
        hbox.getChildren().addAll(kylmkapiSisuListView, ToidudListView, TextBoxRetsept);
        hbox.setPrefHeight(800);
        ToidudListView.setPrefWidth(500);
        kylmkapiSisuListView.setPrefWidth(450);
        Button1.setPrefWidth(primaryStage.getMaxWidth());


        label1.setText("Lisa retsepte");
        TextBoxSisesta1.setPromptText("Mis sul külmkapis on? ");
        TextBoxRetsept.setPromptText("Siin näed retsepti");
        Button1.setText("Sisesta toiduaine");

        vbox.getChildren().addAll(hbox, TextBoxSisesta1, Button1, flow);

        primaryStage.setTitle("Mida Süüa?");
        primaryStage.show();


        hyperlink.setOnAction(event -> {
            // Application.launch(LisaToite.class);

            //create an object of the class you wish to invoke its
            //start() method:

            LisaToite ctc = new LisaToite();

            // Then call its start() method in the following way:

            try {
                ctc.start(LisaToite.classStage);
                primaryStage.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        Button1.setOnAction((event) -> {
            kylmkapiSisu.add(TextBoxSisesta1.getText().toLowerCase());
            kylmkapiSisuFX.clear();
            for (String toiduaine : kylmkapiSisu) {
                kylmkapiSisuFX.add(toiduaine);
            }
            kylmkapiSisuListView.setItems(kylmkapiSisuFX);
            TextBoxSisesta1.clear();
            try {
                ToidudFX.clear();
                ToidudFX.addAll(MidasyyaBG.otsiToite(kylmkapiSisu));
                ToidudListView.setItems(ToidudFX);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        });

        TextBoxSisesta1.setOnAction((event) -> {
            Button1.fire();

        });


        ToidudListView.setOnMouseClicked(new EventHandler<MouseEvent>() {


            @Override
            public void handle(MouseEvent event) {
                String ValitudToit = ToidudListView.getSelectionModel().getSelectedItem().toString();
                try {
                    TextBoxRetsept.setText(MidasyyaBG.otsiRetsepti(ValitudToit));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }
}
