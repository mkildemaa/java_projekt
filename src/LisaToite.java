import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;

public class LisaToite extends Application {

    static Stage classStage = new Stage();

    @Override
    public void start(Stage primaryStage) throws Exception {

        LisaToite.classStage = primaryStage;

        VBox vbox = new VBox();
        HBox hbox = new HBox();
        Scene scene = new Scene(vbox, 700, 900);
        primaryStage.setScene(scene);

        Hyperlink hyperlink = new Hyperlink("Tagasi");
        ListView Listview1 = new ListView();
        Label Label1 = new Label();
        TextField TextBoxSisesta1 = new TextField();
        TextField TextBoxSisesta2 = new TextField();
        TextArea TextboxSisesta3 = new TextArea();
        Button Button1 = new Button();
        // hbox.getChildren().addAll(TextBoxSisesta1, TextBoxSisesta2, TextboxSisesta3);

        Listview1.setPrefHeight(700);
        Listview1.setItems(LaeListi(""));
        Label1.setText("Sisesta andmed ja vajuta Enter");
        TextBoxSisesta1.setPromptText("Toit");
        TextBoxSisesta2.setPromptText("toiduained komaga");
        TextboxSisesta3.setPromptText("juhised (kui on)");
        Button1.setText("Sisesta");
        Button1.setPrefWidth(primaryStage.getMaxWidth());

        vbox.getChildren().addAll(Listview1, Label1, TextBoxSisesta1, TextBoxSisesta2, TextboxSisesta3, Button1, hyperlink);

        primaryStage.setTitle("Lisa retsepte");
        primaryStage.show();


        Button1.setOnAction((event) -> {
            String juhised = TextboxSisesta3.getText().replaceAll("\\_", " ");
            juhised = TextboxSisesta3.getText().replaceAll("(\\r|\\n|\\r\\n)+", "\\\\n");
            String Sisestus = TextBoxSisesta1.getText().toLowerCase() + ":" + TextBoxSisesta2.getText().toLowerCase() + "_" + juhised;
            TextBoxSisesta1.clear();
            TextBoxSisesta2.clear();
            TextboxSisesta3.clear();

            Listview1.setItems(LaeListi(Sisestus));
            System.out.println(Sisestus);
        });




        vbox.setSpacing(5);

    }

    public static ObservableList LaeListi(String ridaMidaLisada) {


        if (!"".equals(ridaMidaLisada)) {

            try {
                String add = "\n" + ridaMidaLisada;
                Files.write(Paths.get("retseptid.txt"), add.getBytes(), StandardOpenOption.APPEND);
            } catch (IOException e) {
                //exception handling left as an exercise for the reader

            }
        }

        ObservableList failiread2 =
                FXCollections.observableArrayList();
        ArrayList<String> failiread = Faililugeja.loeFail("retseptid.txt");

        for (String rida : failiread) {
            failiread2.add(rida);
        }
        return failiread2;
    }


}