/**
 * Created by Markus on 2.12.2016.
 */

import java.lang.IllegalStateException;
import java.util.*;

import static java.util.Arrays.asList;

import java.io.File;
import java.io.FileNotFoundException;

public class MidasyyaBG {


    public static void main(String[] args) {

    }


    public static ArrayList<String> otsiToite(ArrayList<String> kylmkapiSisu) throws FileNotFoundException {


        HashMap<String, ArrayList<String>> hmapretseptid = new HashMap<String, ArrayList<String>>();
        HashMap<String, String> hmapretseptid2 = new HashMap<String, String>();
        ArrayList<String> misToidud = new ArrayList<String>();

        Scanner s = new Scanner(new File("retseptid.txt"));


        while (s.hasNextLine()) {

            String[] sl = new String[20];
            ArrayList<String> o = new ArrayList<String>();
            String a = new String();
            String b = new String();
            String c = new String();
            String[] bb = new String[20];
            String[] cc = new String[2];

            bb = s.nextLine().split("\\:");

            // kui on retsepti juhised juures
            if (bb[1].contains("_")) {

                    cc = bb[1].split("\\_");
                    a = cc[0];
                    c = cc[1];


            } else {
                a = bb[1];
                c = "";
            }
            b = bb[0];
            sl = a.split("\\,");
            for (String se : sl) {
                o.add(se);
            }
            hmapretseptid.put(b, o);
            hmapretseptid2.put(b, c);

        }
        s.close();


        int f = 1;
        while (f != 0) {


            for (Map.Entry<String, ArrayList<String>> entry : hmapretseptid.entrySet()) {

                ArrayList<String> toiduaineteNimekiri = entry.getValue();
                String toit = entry.getKey();

                if (kylmkapiSisu.containsAll(toiduaineteNimekiri)) {


                    misToidud.add(toit);
                }

            }

            f = 0;

        }

        return misToidud;
    }


    public static String otsiRetsepti(String toit) throws FileNotFoundException {

        Scanner s = new Scanner(new File("retseptid.txt"));
        String retsept = new String();
        String[] cc = new String[1];
        while (s.hasNextLine()) {
            String Line = s.nextLine();
            if (Line.contains(toit)) {
                if (Line.contains("_")) {
                    cc = Line.split("\\_");

                    return cc[1].replaceAll("\\\\n", "\n");

                } else {
                    return "retsept kahjuks puudub :(";
                }
            }

        }
        s.close();
        return "X";
    }

}
